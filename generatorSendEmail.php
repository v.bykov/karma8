<?php

use PhpAmqpLib\Message\AMQPMessage;

require_once 'setup.php';

$options = getopt('d::', ['delays::']);
$delays = $options['d'] ?? $options['delays'] ?? '0,3';

$queryValidts = get_query_validts(explode(',', $delays));
$sql = <<<SQL
select * from user where  ($queryValidts) and (confirmed = 1 or valid=1); 
SQL;
$sth = $dbh->prepare($sql);
$sth->execute();
$data = $sth->fetchAll(PDO::FETCH_ASSOC);

/** @var AMQPChannel $rabbitChannelSendEmail */
foreach ($data as $key => $value) {
    $msg = new AMQPMessage(json_encode($value));
    $rabbitChannelSendEmail->batch_basic_publish($msg, '', $CONFIG['queue_send_email']);
    if ($key % 1000) {
        $rabbitChannelSendEmail->publish_batch();
    }
}
$rabbitChannelSendEmail->publish_batch();
