drop database if exists karma8;
create database karma8;

# разрешаем хранить 0 в TIMESTAMP
set global sql_mode = 'ALLOW_INVALID_DATES';

CREATE TABLE `user`
(
    `username`  VARCHAR(255),
    `email`     VARCHAR(255) NOT NULL unique,
    `validts`   TIMESTAMP    NOT NULL DEFAULT 0,
    `createdts` TIMESTAMP    NOT NULL DEFAULT now(),
    `confirmed` bool         NOT NULL DEFAULT '0',
    `checked`   bool         NOT NULL DEFAULT '0',
    `valid`     bool         NOT NULL DEFAULT '0',
    INDEX idx (validts, valid, confirmed, checked),
    PRIMARY KEY (`username`)
);