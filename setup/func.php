<?php

function check_email(string $email): bool
{
    return rand(0, 1) === 1;
}

function send_email(string $from, string $to, string $message): void
{
    return;
}

/**
 * Формирует строку вида (validts > date and validts <=date+1 or validts > date1 and validts <=date1+1)
 *
 * delay отступ в днях от текущей даты для которых надо получить подписки
 * @param int[] $delays
 */
function get_query_validts(array $delays): string
{
    $queryValidts = [];
    foreach ($delays as $delay) {
        $queryValidts[] = 'validts > "' . date('Y-m-d', strtotime("now +$delay day")) . '" and validts <= "' . date('Y-m-d', strtotime("now +" . ((int)$delay + 1) . " day")) . '"';
    }

    return $queryValidts ? implode(' or ', $queryValidts) : 'true';
}