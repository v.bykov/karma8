<?php

$CONFIG = [
    'from' => 'from@from.from',
    'queue_check_email' => 'check_email',
    'queue_send_email' => 'send_email',
    'mysql' => [
        'host' => 'localhost',
        'dbname' => 'karma8',
        'user' => 'user',
        'password' => 'password',
    ],
    'rabbit' => [
        'host' => 'localhost',
        'port' => '5672',
        'user' => 'guest',
        'password' => 'guest',
    ],
];