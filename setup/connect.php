<?php
require_once 'config.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;

$dbh = new PDO("mysql:dbname={$CONFIG['mysql']['dbname']};host={$CONFIG['mysql']['host']}", $CONFIG['mysql']['user'], $CONFIG['mysql']['password']);

$rabbitConnect = new AMQPStreamConnection($CONFIG['rabbit']['host'], $CONFIG['rabbit']['port'], $CONFIG['rabbit']['user'], $CONFIG['rabbit']['password']);
$rabbitChannelCheckEmail = $rabbitConnect->channel();
$rabbitChannelCheckEmail->queue_declare($CONFIG['queue_check_email'], false, true, false, false);

$rabbitChannelSendEmail = $rabbitConnect->channel();
$rabbitChannelSendEmail->queue_declare($CONFIG['queue_send_email'], false, true, false, false);


