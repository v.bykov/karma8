<?php

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

require_once 'setup.php';

$callback = function (AMQPMessage $msg) use ($dbh) {
    $message = json_decode($msg->getBody(), true);
    $sql = <<<SQL
select email from user where  username=:username and confirmed = 0 and checked = 0; 
SQL;

    $sth = $dbh->prepare($sql);
    $sth->execute(['username' => $message['username']]);
    $email = $sth->fetch(PDO::FETCH_COLUMN);
    if (!$email) {
        return;
    }

    $valid = check_email($email);
    $sql = <<<SQL
update user set checked=1, valid=:valid where  email=:email; 
SQL;
    $sth = $dbh->prepare($sql);
    $sth->execute(['email' => $email, 'valid' => $valid]);

    $msg->ack();
};

/** @var AMQPChannel $rabbitChannelCheckEmail */
$rabbitChannelCheckEmail->basic_consume($CONFIG['queue_check_email'], '', false, false, false, false, $callback);

while ($rabbitChannelCheckEmail->is_open()) {
    $rabbitChannelCheckEmail->wait();
}
