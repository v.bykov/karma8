<?php

use PhpAmqpLib\Message\AMQPMessage;

require_once 'setup.php';

$options = getopt('d::', ['delays::']);
$delays = $options['d'] ?? $options['delays'] ?? '1,4';

$queryValidts = get_query_validts(explode(',', $delays));
$sql = <<<SQL
select * from user where  ($queryValidts) and confirmed = 0 and checked = 0 and valid = 0 order by createdts asc; 
SQL;

$sth = $dbh->prepare($sql);
$sth->execute();
$data = $sth->fetchAll(PDO::FETCH_ASSOC);
/** @var AMQPChannel $rabbitChannelCheckEmail */
foreach ($data as $key => $value) {
    $msg = new AMQPMessage(json_encode($value));
    $rabbitChannelCheckEmail->batch_basic_publish($msg, '', $CONFIG['queue_check_email']);
    if ($key % 1000) {
        $rabbitChannelCheckEmail->publish_batch();
    }
}
$rabbitChannelCheckEmail->publish_batch();

