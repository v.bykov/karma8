<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/setup/config.php';
require_once __DIR__ . '/setup/connect.php';
require_once __DIR__ . '/setup/func.php';
