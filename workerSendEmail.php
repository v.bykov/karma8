<?php

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

require_once 'setup.php';

$callback = function (AMQPMessage $msg) use ($CONFIG) {
    $body = json_decode($msg->getBody(), true);
    send_email($CONFIG['from'], $body['email'], "{$body['username']}, your subscription is expiring soon");

    $msg->ack();
};

/** @var AMQPChannel $rabbitChannelSendEmail */
$rabbitChannelSendEmail->basic_consume($CONFIG['queue_send_email'], '', false, false, false, false, $callback);

while ($rabbitChannelSendEmail->is_open()) {
    $rabbitChannelSendEmail->wait();
}